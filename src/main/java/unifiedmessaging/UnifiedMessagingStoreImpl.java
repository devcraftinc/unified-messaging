package unifiedmessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.UUID;

public class UnifiedMessagingStoreImpl implements UnifiedMessagingStore {
	private Map<String, User> users;
	private Map<String, String> activeTokens;
	private List<MessageInfo> messages = new ArrayList<>();

	public UnifiedMessagingStoreImpl() {
		this.users = new HashMap<>();
		this.activeTokens = new HashMap<>();
	}

	public synchronized void addUser(User user) throws UsernameAlreadyTaken {
		if (users.containsKey(user.username))
			throw new UsernameAlreadyTaken();
		users.put(user.username, new User(user.username, user.password));
	}

	public void clear() {
		users.clear();
		activeTokens.clear();
		messages.clear();
	}

	@Override
	public synchronized User getUser(String username) {
		return users.get(username);
	}

	@Override
	public synchronized User getActiveUser(String token) {
		String userId = activeTokens.get(token);
		if (userId == null)
			return null;
		User user = users.get(userId);
		return user;
	}
	
	@Override
	public synchronized String getOrCreateSession(String username) throws NoSuchUser {
		verifyUserExists(username);
		String token = getActiveToken(username);
		if (token == null) {
			token = UUID.randomUUID().toString();
			activeTokens.put(token,username);
		}
		return token;
	}

	private String getActiveToken(String username) {
		String token = null;
		for (Entry<String, String> e: activeTokens.entrySet()) {
			if (e.getValue().equals(username))
				token = e.getKey();
			break;
		}
		return token;
	}

	private User verifyUserExists(String username) throws NoSuchUser {
		User user = users.get(username);
		if (user == null) {
			throw new NoSuchUser(username);
		}
		return user;
	}

	@Override
	public synchronized void addMessage(MessageInfo message) throws NoSuchUser {
		verifyUserExists(message.sender);
		verifyUserExists(message.receiver);
		messages.add(message);
	}

	@Override
	public synchronized MessageInfo[] retreiveMessagesForUser(String username) {
		verifyUserExists(username);
		List<MessageInfo> list = messages.stream().filter(m -> m.sender.equals(username) || m.receiver.equals(username)).collect(Collectors.toList());
		return list.toArray(new MessageInfo[list.size()]);
	}
	
	@Override
	public MessageInfo[] retreiveAllMessages() {
		return messages.toArray(new MessageInfo[messages.size()]);
	}
}