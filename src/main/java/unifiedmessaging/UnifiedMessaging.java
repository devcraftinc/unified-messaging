package unifiedmessaging;

import unifiedmessaging.UnifiedMessagingStore.MessageInfo;

public interface UnifiedMessaging {

	void registerNewUser(String username, String password) throws UsernameAlreadyTaken;

	String login(String username, String password) throws InvalidCredentials;

	void sendText(String senderToken, String receiverUsername, String text) throws NoSuchUser, InvalidToken;

	void sendLink(String senderToken, String receiverUsername, String url) throws NoSuchUser, InvalidToken;

	String sendVoice(String senderToken, String receiverUsername, byte[] bytes) throws InvalidToken, NoSuchUser;

	String sendPhoto(String senderToken, String receiverUsername, String caption, byte[] bytes) throws NoSuchUser, InvalidToken;

	byte[] retreiveFile(String activeToken, String filename);

	MessageInfo[] listMessages(String token);

	String[] suggestFriends(String token);

	MessageInfo[] findInappropriateContent(String adminToken);

}