package unifiedmessaging;

import java.util.HashMap;
import java.util.UUID;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	public static void main(String... args) {
		SpringApplication.run(Application.class);
	}
	
	@Bean
	FileStore fileStore() {
		return new FileStore() {

			private HashMap<String, byte[]> files = new HashMap<>();

			@Override
			public String store(byte[] bytes) {
				String key = UUID.randomUUID().toString();
				files.put(key, bytes);
				return key;
			}

			@Override
			public byte[] retreive(String storePath) {
				return files.get(storePath);
			}
		};
	}

	@Bean
	UnifiedMessaging unifiedMessaging(UnifiedMessagingStore store, FileStore fileStore) {
		return new UnifiedMessagingImpl(store, fileStore);
	}

	@Bean
	UnifiedMessagingStore unifiedMessagingStore() {
		return new UnifiedMessagingStoreImpl();
	}

}